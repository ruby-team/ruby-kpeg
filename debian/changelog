ruby-kpeg (1.3.3-1) unstable; urgency=medium

  [ Dominique Dumont ]
  * control: fix Vcs-Git and Vcs-Browser urls

  [ Cédric Boutillier ]
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Dominique Dumont ]
  * control: remove myself (dod) from Uploaders

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Apply multi-arch hints.
    + ruby-kpeg: Add :any qualifier for ruby dependency.
  * Update watch file format version to 4.

  [ Cédric Boutillier ]
  * Add myself to Uploaders:
  * New upstream version 1.3.3
  * refresh packaging files
  * Use the rake method to run the tests
  * Add patches to fix VERSION constant and adapt the path of .kpeg file when
    running autopkgtest
  * Bump Standards-Version to 4.7.0 (no changes needed)

 -- Cédric Boutillier <boutil@debian.org>  Wed, 25 Sep 2024 00:39:32 +0200

ruby-kpeg (1.0.0-1) unstable; urgency=low

  * Initial release (Closes: #744834)

 -- Dominique Dumont <dod@debian.org>  Tue, 15 Apr 2014 09:01:50 +0200
